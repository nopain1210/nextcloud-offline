#!/bin/bash
set -e

# Check env

if [[ -z "${NEXTCLOUD_HOST}" ]]; then
  echo "NEXTCLOUD_HOST env must be specific. Eg: 192.168.1.1:18080"
  exit 1
fi

if [[ -z "${CODE_HOST}" ]]; then
  echo "CODE_HOST env must be specific. Eg: 192.168.1.1:9980"
  exit 1
fi

# Unzip image
echo "Load docker images"
docker load < nextcloud_22.tar.gz
docker load < code_latest.tar.gz
docker load < mariadb_10_6.tar.gz

# up
echo "docker-compose up"
docker-compose up -d

# Waiting for nextcloud up
echo "Wating for next cloud up"

while :
do
  log=$(docker-compose logs nc)
  test "${string#*${log}}" != "Nextcloud was successfully installed" && break
  sleep 3
done

# Set trusted domain
docker-compose exec -u 33 nc /var/www/html/occ config:system:set trusted_domains 3 --value="${NEXTCLOUD_HOST}"

# Waiting for CODE up
echo "Waiting for CODE up"

while :
do
  log=$(docker-compose logs code)
  test "${string#*${log}}" != "Ready to accept connections on port 9980" && break
  sleep 3
done

# Set CODE url
echo "Set CODE url"
docker-compose exec -u 33 nc /var/www/html/occ config:app:set --value "${CODE_HOST}" richdocuments wopi_url
docker-compose exec -u 33 nc /var/www/html/occ richdocuments:activate-config

echo "Next cloud deploy complete to ${NEXTCLOUD_HOST}"
